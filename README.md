# UDX11 Apps

Application presets for [Ubuntu DockerX11](https://gitlab.com/Yarflam/ubuntu-dockerx11).

Select your preset:

-   [Dwarf Fortress 47.05](https://gitlab.com/Yarflam/udx11-apps/-/tree/game-df)

## Authors

-   Yarflam - _initial work_

## License

The project has no license.
