#!/bin/bash

cmd=$1
test=false
container="udx11-apps"

# COMMANDS

if [ "$cmd" == 'start' ]; then
    test=true
    echo -e '@ Start the application\n'
    docker-compose -f ./docker-compose.yml up -d
fi

if [ "$cmd" == 'build' ]; then
    test=true
    echo -e '@ Build the application\n'
    docker-compose -f ./docker-compose.yml up -d --build --force-recreate
fi

if [ "$cmd" == 'dev' ]; then
    test=true
    echo -e '@ Upgrade the application\n'
    git pull
    ./main.sh build
    ./main.sh docker-cleaner
    docker ps -a
    docker logs -f $container
fi

if [ "$cmd" == 'about' ]; then
    test=true
    echo -e '@ Get information about the container\n'
    docker ps -a | grep $container
fi

if [ "$cmd" == 'stop' ]; then
    test=true
    echo -e '@ Stop the application'
    docker rm -f $container
fi

if [ "$cmd" = 'docker-cleaner' ]; then
    test=true
    echo -e '@ Clean the docker environment\n'
    echo '@ Before'
    docker images
    echo -e '\n@ Action'
    docker rmi $(docker images --filter "dangling=true" -q) 2> /dev/null
    echo -e '\n@ After'
    docker images
fi

if [ "$cmd" == 'help' ]; then
    test=true
    echo -e 'Usage: main.sh [COMMAND]\n'
    echo 'Commands:'
    echo -e 'start\t\tstart the application'
    echo -e 'buid\t\tbuild and start the application'
    echo -e 'about\t\tget information about the container'
    echo -e 'stop\t\tstop the application'
    echo -e 'docker-cleaner\tclean up the docker environment'
fi

if [ $test == false ]; then
    echo -e 'Command not found.\nTry "./main.sh help" for more information.'
fi
